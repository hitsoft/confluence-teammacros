# Hitsoft TeamMacros

A set of Atlassian Confluence macros to visualize project and company teams profile data.

Contains three macroses:
* membercard - shows one card for specified Confluence user
* teamcard - shows set of cards for a list of Confluence users
* teamlist - shows list of confluence users

## Compilation

Just use Apache Maven to compile the project.

    mvn clean install

## Usage

### membercard

The simplest way is to just specify the user name. All other information will be taken from user's profile.

    <ac:macro ac:name="membercard">
      <ac:parameter ac:name="userName">professor</ac:parameter>
    </ac:macro>

If you need to override some information with custom values you may do it specifying some additional parameters

    <ac:macro ac:name="membercard">
      <ac:parameter ac:name="userName">professor</ac:parameter>
      <ac:parameter ac:name="displayName">Hubert Farnsword</ac:parameter>
      <ac:parameter ac:name="role">CEO (Planet Express)</ac:parameter>
    </ac:macro>

The macro generates a set of divs floated left, so when you need to end the line of membercards, just use the proper parameter:

    <ac:macro ac:name="membercard">
      <ac:parameter ac:name="userName">professor</ac:parameter>
      <ac:parameter ac:name="isLast">true</ac:parameter>
    </ac:macro>

### teamcard

This macro is just the serie of {membercard} macroses with simplified specification of the team list.

Here is the sample code for the macro:

    <ac:macro ac:name="teamcard">
      <ac:plain-text-body><![CDATA[professor
      leela=Pilot
      bender=Bender=Bender Rodriges
      fry==Philip J. Fry]]></ac:plain-text-body>
    </ac:macro>

You just need to specify a list of team members in the plain-text body of the macro. There are following rules to do it:

1. Members are placed on their own lines
2. If you need to show just data from profile just specify user's name f.e. "professor"
3. If you need to override the role just specify it after "=" sign. F.e. "leela=Pilot"
4. If you need to override just display name use "==" sign. F.e. "fry==Philip J. Fry"
5. If you need to override both role and display name just separate them with "=" sign. F.e. "bender=Bender=Bender Rodriges"

### teamlist

This macro will show the list of team members with teir contacts in the table.

    <ac:macro ac:name="teamlist">
      <ac:plain-text-body><![CDATA[professor
      leela=Pilot
      bender=Bender=Bender Rodriges
      fry==Philip J. Fry]]></ac:plain-text-body>
    </ac:macro>

It uses the same rules for the list of members as described for the {teamcard} macro.
