package com.hitsoft.atlassian.plugin.confluence.teammacros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.user.UserDetailsManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.user.UserManager;

import java.util.Map;

/**
 * This very simple macro shows you the very basic use-case of displaying *something* on the Confluence page where it is used.
 * Use this example macro to toy around, and then quickly move on to the next example - this macro doesn't
 * really show you all the fun stuff you can do with Confluence.
 */
public class MemberCardMacro implements Macro {

    // We just have to define the variables and the setters, then Spring injects the correct objects for us to use. Simple and efficient.
    // You just need to know *what* you want to inject and use.

    private final UserManager userManager;
    private final UserDetailsManager userDetailsManager;
    private final Renderer renderer;
    private static final String TEMPLATE = "com/hitsoft/atlassian/plugin/confluence/teammacros/MemberCardMacro.vm";

    public MemberCardMacro(UserManager userManager, UserDetailsManager userDetailsManager, Renderer renderer) {
        this.userManager = userManager;
        this.userDetailsManager = userDetailsManager;
        this.renderer = renderer;
    }


    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        String userName = parameters.get("userName");
        String displayName = parameters.get("displayName");
        String role = parameters.get("role");
        boolean isLast = Boolean.parseBoolean(parameters.get("isLast"));

        Map<String, Object> ctx = MacroUtils.defaultVelocityContext();
        ctx.put("model", TeamUtils.loadUser(userName, displayName, role, isLast, userManager, userDetailsManager));

        return renderer.render(VelocityUtils.getRenderedTemplate(TEMPLATE, ctx), context);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }

}
