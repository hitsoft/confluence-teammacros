package com.hitsoft.atlassian.plugin.confluence.teammacros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import java.util.Map;

/**
 * This very simple macro shows you the very basic use-case of displaying *something* on the Confluence page where it is used.
 * Use this example macro to toy around, and then quickly move on to the next example - this macro doesn't
 * really show you all the fun stuff you can do with Confluence.
 */
public class TeamCardMacro implements Macro {

    // We just have to define the variables and the setters, then Spring injects the correct objects for us to use. Simple and efficient.
    // You just need to know *what* you want to inject and use.

    private static final String TEMPLATE = "com/hitsoft/atlassian/plugin/confluence/teammacros/TeamCardMacro.vm";
    private final Renderer renderer;

    public TeamCardMacro(Renderer renderer) {
        this.renderer = renderer;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        Map<String, Object> ctx = MacroUtils.defaultVelocityContext();
        ctx.put("memberList", TeamUtils.parseBody(body));
        return renderer.render(VelocityUtils.getRenderedTemplate(TEMPLATE, ctx), context);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
