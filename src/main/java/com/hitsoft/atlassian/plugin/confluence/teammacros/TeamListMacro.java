package com.hitsoft.atlassian.plugin.confluence.teammacros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.user.UserDetailsManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.user.UserManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This very simple macro shows you the very basic use-case of displaying *something* on the Confluence page where it is used.
 * Use this example macro to toy around, and then quickly move on to the next example - this macro doesn't
 * really show you all the fun stuff you can do with Confluence.
 */
public class TeamListMacro implements Macro {

    // We just have to define the variables and the setters, then Spring injects the correct objects for us to use. Simple and efficient.
    // You just need to know *what* you want to inject and use.

    private final UserManager userManager;
    private final UserDetailsManager userDetailsManager;
    private final Renderer renderer;
    private static final String TEMPLATE = "com/hitsoft/atlassian/plugin/confluence/teammacros/TeamListMacro.vm";

    public TeamListMacro(UserManager userManager, UserDetailsManager userDetailsManager, Renderer renderer) {
        this.userManager = userManager;
        this.userDetailsManager = userDetailsManager;
        this.renderer = renderer;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        List<TeamUtils.UserModel> userList = new ArrayList<TeamUtils.UserModel>();
        for (TeamUtils.Member member : TeamUtils.parseBody(body)) {
            userList.add(TeamUtils.loadUser(member.userName, member.displayName, member.role, false, userManager, userDetailsManager));
        }
        Map<String, Object> ctx = MacroUtils.defaultVelocityContext();
        ctx.put("userList", userList);
        return renderer.render(VelocityUtils.getRenderedTemplate(TEMPLATE, ctx), context);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
