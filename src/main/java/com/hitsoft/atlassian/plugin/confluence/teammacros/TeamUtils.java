package com.hitsoft.atlassian.plugin.confluence.teammacros;

import com.atlassian.confluence.user.UserDetailsManager;
import com.atlassian.user.EntityException;
import com.atlassian.user.User;
import com.atlassian.user.UserManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: smeagol
 * Date: 04.08.12
 * Time: 10:09
 */
public class TeamUtils {

    public static class Member {
        String userName = null;
        String displayName = null;
        String role = null;

        public String getDisplayName() {
            return displayName;
        }

        public String getRole() {
            return role;
        }

        public String getUserName() {
            return userName;
        }
    }

    public static class UserModel {

        public static class IMModel {
            String kind;
            String value;

            public String getKind() {
                return kind;
            }

            public String getValue() {
                return value;
            }
        }

        boolean isUserExists = false;
        String userName = null;
        String displayName = null;
        String role = null;
        String phone = null;
        String email = null;
        List<IMModel> im = new ArrayList<IMModel>();
        boolean isLast = false;

        public String getDisplayName() {
            return displayName;
        }

        public String getEmail() {
            return email;
        }

        public List<IMModel> getIm() {
            return im;
        }

        public boolean getIsUserExists() {
            return isUserExists;
        }

        public String getRole() {
            return role;
        }

        public String getUserName() {
            return userName;
        }

        public String getPhone() {
            return phone;
        }

        public boolean getIsLast() {
            return isLast;
        }
    }

    public static List<Member> parseBody(String body) {
        List<Member> result = new ArrayList<Member>();
        for (String line : body.split("\n")) {
            if (line.length() > 0) {
                String[] items = line.split("=");
                Member res = new Member();
                if (items.length > 0) {
                    res.userName = items[0];
                }
                if (items.length > 1) {
                    res.role = items[1];
                    if (res.role.isEmpty())
                        res.role = "";
                }
                if (items.length > 2) {
                    res.displayName = items[2];
                    if (res.displayName.isEmpty())
                        res.displayName = "";
                }
                result.add(res);
            }
        }
        return result;
    }

    public static UserModel loadUser(String userName, String displayName, String role, boolean isLast, UserManager userManager, UserDetailsManager userDetailsManager) {
        UserModel result = new UserModel();
        result.userName = userName;
        result.displayName = displayName;
        result.role = role;
        result.isLast = isLast;
        try {
            User user = userManager.getUser(result.userName);
            result.isUserExists = (user != null);
            if (result.isUserExists) {
                assert user != null;
                if (result.displayName == null) {
                    result.displayName = user.getFullName();
                }
                if (result.role == null) {
                    String position = userDetailsManager.getStringProperty(user, "position");
                    String department = userDetailsManager.getStringProperty(user, "department");
                    if (position != null) {
                        result.role = position;
                        if (department != null)
                            result.role = String.format("%s (%s)", position, department);
                    }
                }
                result.email = user.getEmail();
                String im = userDetailsManager.getStringProperty(user, "im");
                if (im != null) {
                    for (String imItem : im.split(",")) {
                        String item = imItem.trim();
                        if (!item.isEmpty()) {
                            String[] val = item.split(":");
                            if (val.length == 2) {
                                UserModel.IMModel imModel = new UserModel.IMModel();
                                imModel.kind = val[0].trim();
                                imModel.value = val[1].trim();
                                result.im.add(imModel);
                            }
                        }
                    }
                }
                result.phone = userDetailsManager.getStringProperty(user, "phone");
            }
        } catch (EntityException e) {
            result.isUserExists = false;
        }
        return result;
    }
}
